Example Springboot Application for checking kW/h consumption by user. External api provide data in xml. (Api is a little bit 'break' - it return more data than excepted).
Example "Frontend" is realized by Vaadin in order for convenient checking how it works, it was not main purpose of task. 
"Frontend" could be boldly extracted for another project. The application communicates with these parts as if it were almost separate application :)

How to run it in IDE:
- Setup sdk : Java 12
- Install Lombok plugin
- Enable annotation processing

start - yyyy-MM-dd
end - yyyy-MM-dd

Example Request to api:
http://localhost:8080/report?start=2019-09-01&end=2019-09-14

#Open app: localhost:8080/home
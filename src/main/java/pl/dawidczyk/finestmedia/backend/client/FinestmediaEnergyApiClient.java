package pl.dawidczyk.finestmedia.backend.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dawidczyk.finestmedia.common.model.EnergyReport;

import java.time.LocalDate;

import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

@FeignClient(name = "FinestMediaEnergyApiClient",
        url = "https://finestmedia.ee/kwh/",
        configuration = FinestmediaEnergyApiClient.ResponseMapperConfiguration.class)
public interface FinestmediaEnergyApiClient {

    @GetMapping
    EnergyReport getEnergyReport(@RequestParam("start") final LocalDate start, @RequestParam("end") final LocalDate end);

    class ResponseMapperConfiguration {
        @Bean
        ObjectMapper objectMapper() {
            return new XmlMapper()
                    .registerModule(new JavaTimeModule())
                    .configure(WRITE_DATES_AS_TIMESTAMPS, false);
        }
    }

}

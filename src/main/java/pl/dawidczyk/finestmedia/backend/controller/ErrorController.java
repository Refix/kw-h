package pl.dawidczyk.finestmedia.backend.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientException;
import pl.dawidczyk.finestmedia.backend.controller.model.Error;

import static pl.dawidczyk.finestmedia.backend.controller.model.ErrorMessageConst.IS_MORE_THAN_TWO_YEARS_OF_RANGE_OR_INVALID_PARAMS;

@ControllerAdvice
public class ErrorController {

    @ExceptionHandler(RestClientException.class)
    public ResponseEntity<Error> handleRestClientException(final RestClientException exception) {
        return ResponseEntity
                .badRequest()
                .contentType(MediaType.APPLICATION_JSON)
                .body(Error.of(IS_MORE_THAN_TWO_YEARS_OF_RANGE_OR_INVALID_PARAMS, exception.getMessage()));
    }

}

package pl.dawidczyk.finestmedia.backend.controller;

import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.dawidczyk.finestmedia.common.model.AggregatedEnergyReport;
import pl.dawidczyk.finestmedia.backend.service.MonthlyWeeklyConsumptionService;

import java.time.LocalDate;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/report")
@AllArgsConstructor
public class EnergyReportController {

    private final MonthlyWeeklyConsumptionService monthlyWeeklyConsumptionService;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public AggregatedEnergyReport energyReport(@RequestParam @DateTimeFormat(iso = DATE) final LocalDate start,
                                               @RequestParam @DateTimeFormat(iso = DATE) final LocalDate end) {
        return monthlyWeeklyConsumptionService.getAggregatedEnergyReport(start, end);
    }

}

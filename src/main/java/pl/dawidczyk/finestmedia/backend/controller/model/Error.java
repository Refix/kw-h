package pl.dawidczyk.finestmedia.backend.controller.model;

import lombok.Value;

@Value(staticConstructor = "of")
public class Error {
    String reason;
    String exceptionMessage;
}

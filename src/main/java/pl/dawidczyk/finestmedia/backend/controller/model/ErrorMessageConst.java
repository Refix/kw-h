package pl.dawidczyk.finestmedia.backend.controller.model;

public class ErrorMessageConst {
    public static final String IS_MORE_THAN_TWO_YEARS_OF_RANGE_OR_INVALID_PARAMS = "Probably no data for the given range - maximum 2 years from yesterday, or invalid params (YYYY-MM-DD)";
}

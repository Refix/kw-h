package pl.dawidczyk.finestmedia.backend.service;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import pl.dawidczyk.finestmedia.common.model.AggregatedEnergyReport;
import pl.dawidczyk.finestmedia.common.model.DailyConsumption;
import pl.dawidczyk.finestmedia.common.model.MonthlyConsumption;
import pl.dawidczyk.finestmedia.common.model.WeeklyConsumption;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.WeekFields;
import java.util.List;

import static java.util.Locale.getDefault;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class MonthlyWeeklyConsumptionService {

    private final DailyConsumptionService dailyConsumptionService;

    @Cacheable("energy-report")
    public AggregatedEnergyReport getAggregatedEnergyReport(@NonNull final LocalDate start,
                                                            @NonNull final LocalDate end) {
        return AggregatedEnergyReport.builder()
                .monthlyConsumptions(getMonthlyConsumptions(start, end))
                .build();
    }

    private List<MonthlyConsumption> getMonthlyConsumptions(@NonNull final LocalDate start,
                                                            @NonNull final LocalDate end) {
        return dailyConsumptionService.getDailyConsumptions(start, end)
                .stream()
                .collect(groupingBy(it -> YearMonth.of(it.getDate().getYear(), it.getDate().getMonth())))
                .entrySet()
                .stream()
                .map(it -> MonthlyConsumption.builder()
                        .yearMonth(it.getKey())
                        .weeklyConsumptions(getWeeklyConsumptions(it.getValue()))
                        .build())
                .collect(toList());
    }

    private List<WeeklyConsumption> getWeeklyConsumptions(@NonNull final List<DailyConsumption> dailyConsumptions) {
        return dailyConsumptions
                .stream()
                .collect(groupingBy(it -> it.getDate().get(WeekFields.of(getDefault()).weekOfMonth())))
                .entrySet()
                .stream()
                .map(it -> WeeklyConsumption.builder()
                        .weekNumber(it.getKey())
                        .yearMonth(YearMonth.of(it.getValue().get(0).getDate().getYear(), it.getValue().get(0).getDate().getMonth()))
                        .dailyConsumptions(it.getValue())
                        .build())
                .collect(toList());
    }
}

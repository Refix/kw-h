package pl.dawidczyk.finestmedia.backend.service;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Service;
import pl.dawidczyk.finestmedia.backend.client.FinestmediaEnergyApiClient;
import pl.dawidczyk.finestmedia.common.model.DailyConsumption;
import pl.dawidczyk.finestmedia.common.model.HourConsumption;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

import static java.time.ZoneId.systemDefault;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Service
@AllArgsConstructor
public class DailyConsumptionService {

    private final FinestmediaEnergyApiClient finestmediaEnergyApiClient;

    List<DailyConsumption> getDailyConsumptions(@NonNull final LocalDate start,
                                                @NonNull final LocalDate end) {
        return getConsumptionPerDay(start, end)
                .entrySet()
                .stream()
                .map(it -> DailyConsumption.builder()
                        .consumption(it.getValue())
                        .date(it.getKey())
                        .build())
                .sorted(comparing(DailyConsumption::getDate))
                .collect(toList());
    }

    private Map<LocalDate, BigDecimal> getConsumptionPerDay(@NonNull final LocalDate start,
                                                            @NonNull final LocalDate end) {
        return finestmediaEnergyApiClient.getEnergyReport(start, end)
                .getAccountTimeSeries()
                .getHourConsumption()
                .stream()
                .filter(it -> dateIsInRange(it.getDate().withZoneSameLocal(systemDefault()), start, end))
                .collect(toMap(it -> it.getDate().toLocalDate(), HourConsumption::getConsumption, BigDecimal::add));
    }

    private boolean dateIsInRange(@NonNull final ZonedDateTime dateTime,
                                  @NonNull final LocalDate start,
                                  @NonNull final LocalDate end) {
        return dateTime.isBefore(end.plusDays(1).atStartOfDay(systemDefault())) &&
                dateTime.isAfter(start.atStartOfDay(systemDefault()));
    }

}

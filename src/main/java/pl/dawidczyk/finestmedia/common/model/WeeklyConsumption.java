package pl.dawidczyk.finestmedia.common.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.YearMonth;
import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Value
@Builder(toBuilder = true)
@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE, force = true)
public class WeeklyConsumption {
    YearMonth yearMonth;
    Integer weekNumber;
    List<DailyConsumption> dailyConsumptions;
}

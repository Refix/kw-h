package pl.dawidczyk.finestmedia.common.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Value
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = PRIVATE, force = true)
public class AggregatedEnergyReport {
    List<MonthlyConsumption> monthlyConsumptions;
}

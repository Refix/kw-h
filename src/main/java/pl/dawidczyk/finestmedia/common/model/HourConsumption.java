package pl.dawidczyk.finestmedia.common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import static lombok.AccessLevel.PRIVATE;

@Value
@Builder(toBuilder = true)
@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE, force = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HourConsumption {
    @JacksonXmlProperty(localName = "ts", isAttribute = true)
    ZonedDateTime date;
    @JacksonXmlText
    BigDecimal consumption;
}


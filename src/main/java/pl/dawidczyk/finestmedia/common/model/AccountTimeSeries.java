package pl.dawidczyk.finestmedia.common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Value
@Builder
@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE, force = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountTimeSeries {
    @JacksonXmlElementWrapper(localName = "ConsumptionHistory")
    @JacksonXmlProperty(localName = "HourConsumption")
    List<HourConsumption> hourConsumption;
}
package pl.dawidczyk.finestmedia.frontend.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dawidczyk.finestmedia.common.model.AggregatedEnergyReport;
import java.time.LocalDate;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;

@FeignClient(name = "EnergyReportApiClient",
        url = "http://localhost:8080/report")
public interface MyEnergyReportApiClient {
    @GetMapping
    AggregatedEnergyReport getAggregatedEnergyReport(@RequestParam("start") @DateTimeFormat(iso = DATE) final LocalDate start,
                                                     @RequestParam("end") @DateTimeFormat(iso = DATE) final LocalDate end);
}

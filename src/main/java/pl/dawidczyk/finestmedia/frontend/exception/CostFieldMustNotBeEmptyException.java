package pl.dawidczyk.finestmedia.frontend.exception;

import static pl.dawidczyk.finestmedia.frontend.util.CustomExceptionMessages.COST_TEXT_FIELD_IS_EMPTY_ERROR_MESSAGE;

public class CostFieldMustNotBeEmptyException extends Exception {
    public CostFieldMustNotBeEmptyException() {
        super(COST_TEXT_FIELD_IS_EMPTY_ERROR_MESSAGE);
    }
}

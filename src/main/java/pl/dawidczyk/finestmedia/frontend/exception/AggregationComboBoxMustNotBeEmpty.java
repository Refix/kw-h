package pl.dawidczyk.finestmedia.frontend.exception;

import static pl.dawidczyk.finestmedia.frontend.util.CustomExceptionMessages.AGGREGATION_COMBO_BOX_MUST_NOT_BE_EMPTY;

public class AggregationComboBoxMustNotBeEmpty extends Exception {
    public AggregationComboBoxMustNotBeEmpty() {
        super(AGGREGATION_COMBO_BOX_MUST_NOT_BE_EMPTY);
    }
}

package pl.dawidczyk.finestmedia.frontend.exception;

import static pl.dawidczyk.finestmedia.frontend.util.CustomExceptionMessages.ANY_OF_DATE_PICKERS_IS_EMPTY_ERROR_MESSAGE;

public class DatePickersMustNotBeEmptyException extends Exception {
    public DatePickersMustNotBeEmptyException(){
        super(ANY_OF_DATE_PICKERS_IS_EMPTY_ERROR_MESSAGE);
    }
}

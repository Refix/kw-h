package pl.dawidczyk.finestmedia.frontend.exception;

import static pl.dawidczyk.finestmedia.frontend.util.CustomExceptionMessages.START_DATE_MUST_BE_BEFORE_OR_EQUAL_END_DATE;

public class StartDateMustBeBeforeOrEqualEndDateException extends Exception {
    public StartDateMustBeBeforeOrEqualEndDateException() {
        super(START_DATE_MUST_BE_BEFORE_OR_EQUAL_END_DATE);
    }
}

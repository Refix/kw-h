package pl.dawidczyk.finestmedia.frontend.exception;

import static pl.dawidczyk.finestmedia.frontend.util.CustomExceptionMessages.IS_NOT_A_NUMBER_ERROR_MESSAGE;

public class CostTextFieldValueMustBeANumberException extends Exception {
    public CostTextFieldValueMustBeANumberException() {
        super(IS_NOT_A_NUMBER_ERROR_MESSAGE);
    }
}

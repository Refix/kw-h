package pl.dawidczyk.finestmedia.frontend;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.springframework.beans.factory.annotation.Autowired;
import pl.dawidczyk.finestmedia.frontend.model.EnergyReportResult;
import pl.dawidczyk.finestmedia.frontend.service.UiService;
import pl.dawidczyk.finestmedia.frontend.util.Validator;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static com.vaadin.flow.component.notification.Notification.Position.BOTTOM_CENTER;
import static com.vaadin.flow.component.notification.Notification.show;
import static pl.dawidczyk.finestmedia.frontend.util.CustomCaption.*;

@Route(value = "home")
@Theme(value = Lumo.class)
public class HomePage extends VerticalLayout {

    private static final int NUMBER_FIELD_MAX_LENGTH = 10;
    private static final int NOTIFICATION_DURATION_VALUE = 2000;
    private static final List<String> aggregationTypes = Arrays.asList(DAILY, WEEKLY, MONTHLY);

    private final UiService uiService;

    private HorizontalLayout searchPanelHorizontalLayout;
    private Grid<EnergyReportResult> energyReportResultGrid;
    private Button checkConsumptionButton;
    private Button resetButton;
    private DatePicker startDatePicker;
    private DatePicker endDatePicker;
    private ComboBox<String> aggregationTypeComboBox;

    private NumberField costPerKWHNumberField;
    private Checkbox isCostAllowedCheckbox;

    @Autowired
    public HomePage(UiService uiService) {
        this.uiService = uiService;
        configureComponents();
    }

    private void configureComponents() {
        startDatePicker = getConfiguredStartDatePicker();
        endDatePicker = getConfiguredEndDatePicker();
        aggregationTypeComboBox = getConfiguredAggregationTypeComboBox();
        isCostAllowedCheckbox = getConfiguredIsCostAllowedCheckBox();
        costPerKWHNumberField = getConfiguredCostPerKWHNumberField();
        resetButton = getConfiguredResetButton();
        checkConsumptionButton = getConfiguredCheckConsumptionButton();
        searchPanelHorizontalLayout = getConfiguredSearchPanelHorizontalLayout();
        energyReportResultGrid = getConfiguredEnergyReportGrid();

        add(searchPanelHorizontalLayout);
        add(energyReportResultGrid);
        setSizeFull();
    }

    private HorizontalLayout getConfiguredSearchPanelHorizontalLayout() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.add(startDatePicker);
        layout.add(endDatePicker);
        layout.add(aggregationTypeComboBox);
        layout.add(costPerKWHNumberField);
        layout.add(isCostAllowedCheckbox);
        layout.add(checkConsumptionButton);
        layout.add(resetButton);
        layout.setWidthFull();

        return layout;
    }

    private Button getConfiguredResetButton() {
        Button button = new Button(RESET);
        button.setThemeName(Lumo.DARK);
        button.addClickListener(event -> resetComponents());


        return button;
    }

    private Button getConfiguredCheckConsumptionButton() {
        Button button = new Button(CHECK_CONSUMPTION);
        button.setThemeName(Lumo.DARK);
        button.setWidthFull();
        button.addClickShortcut(Key.ENTER);
        button.addClickListener(event -> buttonAction());

        return button;
    }

    private DatePicker getConfiguredStartDatePicker() {
        DatePicker datePicker = new DatePicker();
        datePicker.setPlaceholder(START_DATE);
        datePicker.setMax(LocalDate.now().minusDays(1));
        datePicker.setMin(LocalDate.now().minusYears(2));
        datePicker.setErrorMessage(DATE_NOT_ALLOWED);

        return datePicker;
    }

    private DatePicker getConfiguredEndDatePicker() {
        DatePicker datePicker = new DatePicker();
        datePicker.setPlaceholder(END_DATE);
        datePicker.setMax(LocalDate.now().minusDays(1));
        datePicker.setMin(LocalDate.now().minusYears(2));
        datePicker.setErrorMessage(DATE_NOT_ALLOWED);

        return datePicker;
    }

    private ComboBox<String> getConfiguredAggregationTypeComboBox() {
        ComboBox comboBox = new ComboBox();
        comboBox.setItems(aggregationTypes);
        comboBox.setValue(aggregationTypes.get(0));

        return comboBox;
    }

    private Checkbox getConfiguredIsCostAllowedCheckBox() {
        Checkbox checkbox = new Checkbox(CALCULATE_COST);
        checkbox.addValueChangeListener(event -> {
            if (checkbox.getValue()) costPerKWHNumberField.setEnabled(true);
            else costPerKWHNumberField.setEnabled(false);
            costPerKWHNumberField.clear();

        });
        return checkbox;
    }

    private NumberField getConfiguredCostPerKWHNumberField() {
        NumberField numberField = new NumberField();
        numberField.setWidth("230px");
        numberField.setEnabled(false);
        numberField.setHasControls(true);
        numberField.setStep(0.5d);
        numberField.setMin(0);
        numberField.setMaxLength(NUMBER_FIELD_MAX_LENGTH);
        numberField.setPlaceholder(COST_PER_KWH);

        return numberField;
    }

    private Grid<EnergyReportResult> getConfiguredEnergyReportGrid() {
        Grid<EnergyReportResult> grid = new Grid<>();
        grid.setThemeName(Lumo.LIGHT);
        grid.setSizeFull();
        return grid;
    }

    private void buttonAction() {
        try {
            validateFields();
            checkConsumption();
        } catch (Exception e) {
            show(e.getMessage(), NOTIFICATION_DURATION_VALUE, BOTTOM_CENTER);
            e.printStackTrace();
        }
    }

    private void validateFields() throws Exception {
        Validator.checkIfDatePickersAreEmpty(startDatePicker, endDatePicker);
        Validator.checkIfStartDateIsBeforeOrEqualEndDate(startDatePicker.getValue(), endDatePicker.getValue());
        Validator.checkIfAggregationFieldIsEmpty(aggregationTypeComboBox);
        if (isCostAllowedCheckbox.getValue())
            Validator.checkIfCostFieldIsEmpty(costPerKWHNumberField);
    }

    private void checkConsumption() {
        if (isCostAllowedCheckbox.getValue()) {
            configureColumnsWithCost();
            BigDecimal cost = BigDecimal.valueOf(costPerKWHNumberField.getValue());
            if (aggregationTypeComboBox.getValue().equals(DAILY))
                energyReportResultGrid.setItems(uiService.getEnergyReportResultsWithCalculatedCost(cost, getDailyEnergyReportResult()));
            if (aggregationTypeComboBox.getValue().equals(WEEKLY))
                energyReportResultGrid.setItems(uiService.getEnergyReportResultsWithCalculatedCost(cost, getWeeklyEnergyReportResult()));
            if (aggregationTypeComboBox.getValue().equals(MONTHLY))
                energyReportResultGrid.setItems(uiService.getEnergyReportResultsWithCalculatedCost(cost, getMonthlyEnergyReportResult()));
        } else {
            configureColumnsWithoutCost();
            if (aggregationTypeComboBox.getValue().equals(DAILY))
                energyReportResultGrid.setItems(getDailyEnergyReportResult());
            if (aggregationTypeComboBox.getValue().equals(WEEKLY))
                energyReportResultGrid.setItems(getWeeklyEnergyReportResult());
            if (aggregationTypeComboBox.getValue().equals(MONTHLY))
                energyReportResultGrid.setItems(getMonthlyEnergyReportResult());
        }
    }

    private void configureColumnsWithoutCost() {
        energyReportResultGrid.removeAllColumns();
        energyReportResultGrid.addColumn(EnergyReportResult::getPeriod).setHeader(PERIOD).setWidth("70%");
        energyReportResultGrid.addColumn(EnergyReportResult::getValueOfConsumption).setHeader(KWH).setWidth("30%");
    }

    private void configureColumnsWithCost() {
        energyReportResultGrid.removeAllColumns();
        energyReportResultGrid.addColumn(EnergyReportResult::getPeriod).setHeader(PERIOD).setWidth("70%");
        energyReportResultGrid.addColumn(EnergyReportResult::getValueOfConsumption).setHeader(KWH).setWidth("15%");
        energyReportResultGrid.addColumn(EnergyReportResult::getCost).setHeader(COST).setWidth("15%");
    }

    private List<EnergyReportResult> getDailyEnergyReportResult() {
        return uiService.getDailyEnergyReportResults(
                startDatePicker.getValue(),
                endDatePicker.getValue()
        );
    }

    private List<EnergyReportResult> getWeeklyEnergyReportResult() {
        return uiService.getWeeklyEnergyReportResults(
                startDatePicker.getValue(),
                endDatePicker.getValue()
        );
    }

    private List<EnergyReportResult> getMonthlyEnergyReportResult() {
        return uiService.getMonthlyEnergyReportResults(
                startDatePicker.getValue(),
                endDatePicker.getValue()
        );
    }

    private void resetComponents() {
        energyReportResultGrid.removeAllColumns();
        endDatePicker.clear();
        startDatePicker.clear();
        costPerKWHNumberField.clear();
        costPerKWHNumberField.setEnabled(false);
        isCostAllowedCheckbox.setValue(false);
        aggregationTypeComboBox.setValue(DAILY);
    }
}

package pl.dawidczyk.finestmedia.frontend.util;

public class CustomExceptionMessages {
    public static final String START_DATE_MUST_BE_BEFORE_OR_EQUAL_END_DATE = "ERROR: Start date must be before or equal end date.";
    public static final String ANY_OF_DATE_PICKERS_IS_EMPTY_ERROR_MESSAGE = "ERROR: Date pickers must not be empty.";
    public static final String COST_TEXT_FIELD_IS_EMPTY_ERROR_MESSAGE = "ERROR: Cost field must not be empty.";
    public static final String IS_NOT_A_NUMBER_ERROR_MESSAGE = "ERROR: Cost field require a number :)";
    public static final String AGGREGATION_COMBO_BOX_MUST_NOT_BE_EMPTY = "ERROR: Aggregation field must not be empty.";
}

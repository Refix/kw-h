package pl.dawidczyk.finestmedia.frontend.util;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.textfield.NumberField;
import lombok.NonNull;
import pl.dawidczyk.finestmedia.frontend.exception.AggregationComboBoxMustNotBeEmpty;
import pl.dawidczyk.finestmedia.frontend.exception.CostFieldMustNotBeEmptyException;
import pl.dawidczyk.finestmedia.frontend.exception.DatePickersMustNotBeEmptyException;
import pl.dawidczyk.finestmedia.frontend.exception.StartDateMustBeBeforeOrEqualEndDateException;

import java.time.LocalDate;

public class Validator {
    public static void checkIfDatePickersAreEmpty(@NonNull final DatePicker... datePickers) throws DatePickersMustNotBeEmptyException {
        if (isEmptyAnyOfDatePickersFields(datePickers)) throw new DatePickersMustNotBeEmptyException();
    }

    public static void checkIfCostFieldIsEmpty(@NonNull final NumberField numberField) throws CostFieldMustNotBeEmptyException {
        if (numberField.isEmpty()) throw new CostFieldMustNotBeEmptyException();
    }

    public static void checkIfStartDateIsBeforeOrEqualEndDate(@NonNull final LocalDate start,
                                                              @NonNull final LocalDate end) throws StartDateMustBeBeforeOrEqualEndDateException {
        if (start.isAfter(end)) throw new StartDateMustBeBeforeOrEqualEndDateException();
    }

    public static void checkIfAggregationFieldIsEmpty(@NonNull final ComboBox comboBox) throws AggregationComboBoxMustNotBeEmpty {
        if (comboBox.isEmpty()) throw new AggregationComboBoxMustNotBeEmpty();
    }

    private static boolean isEmptyAnyOfDatePickersFields(@NonNull final DatePicker... datePickers) {
        boolean valid = false;
        for (DatePicker datePicker : datePickers) {
            if (datePicker.isEmpty()) {
                valid = true;
                break;
            }
        }
        return valid;
    }
}

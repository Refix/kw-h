package pl.dawidczyk.finestmedia.frontend.util;

public class CustomCaption {
    public static final String COST_PER_KWH = "Cost per kW/h";
    public static final String START_DATE = "Start date";
    public static final String END_DATE = "End date";
    public static final String PERIOD = "Period";
    public static final String KWH = "kW/h";
    public static final String COST = "Cost";
    public static final String CHECK_CONSUMPTION = "Check consumption";
    public static final String DATE_NOT_ALLOWED = "Date not allowed!";
    public static final String RESET = "Reset";
    public static final String CALCULATE_COST = "Calculate cost";

    public static final String DAILY = "DAILY";
    public static final String WEEKLY = "WEEKLY";
    public static final String MONTHLY = "MONTHLY";
}

package pl.dawidczyk.finestmedia.frontend.service;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Service;
import pl.dawidczyk.finestmedia.common.model.DailyConsumption;
import pl.dawidczyk.finestmedia.common.model.WeeklyConsumption;
import pl.dawidczyk.finestmedia.frontend.client.MyEnergyReportApiClient;
import pl.dawidczyk.finestmedia.frontend.model.EnergyReportResult;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class UiService {

    private final MyEnergyReportApiClient myEnergyReportApiClient;

    public List<EnergyReportResult> getMonthlyEnergyReportResults(@NonNull final LocalDate start,
                                                                  @NonNull final LocalDate end) {
        return myEnergyReportApiClient.getAggregatedEnergyReport(start, end).getMonthlyConsumptions()
                .stream()
                .map(it -> EnergyReportResult.builder()
                        .period(buildMonthlyInfoString(it.getYearMonth()))
                        .valueOfConsumption(sumWeeklyConsumptions(it.getWeeklyConsumptions()))
                        .build())
                .sorted(comparing(EnergyReportResult::getPeriod))
                .collect(toList());
    }

    public List<EnergyReportResult> getWeeklyEnergyReportResults(@NonNull final LocalDate start,
                                                                 @NonNull final LocalDate end) {
        return myEnergyReportApiClient.getAggregatedEnergyReport(start, end).getMonthlyConsumptions()
                .stream()
                .map(it -> it.getWeeklyConsumptions()
                        .stream()
                        .map(weeklyConsumption -> EnergyReportResult.builder()
                                .period(buildWeeklyInfoString(weeklyConsumption.getYearMonth(), weeklyConsumption.getWeekNumber()))
                                .valueOfConsumption(sumDailyConsumptions(weeklyConsumption.getDailyConsumptions()))
                                .build())
                        .collect(toList()))
                .collect(toList())
                .stream()
                .flatMap(List::stream)
                .sorted(comparing(EnergyReportResult::getPeriod))
                .collect(toList());
    }

    public List<EnergyReportResult> getDailyEnergyReportResults(@NonNull final LocalDate start,
                                                                @NonNull final LocalDate end) {
        return myEnergyReportApiClient.getAggregatedEnergyReport(start, end).getMonthlyConsumptions()
                .stream()
                .map(monthlyConsumption -> monthlyConsumption.getWeeklyConsumptions()
                        .stream()
                        .map(weeklyConsumption -> weeklyConsumption.getDailyConsumptions()
                                .stream()
                                .map(dailyConsumption -> EnergyReportResult.builder()
                                        .period(dailyConsumption.getDate().toString())
                                        .valueOfConsumption(dailyConsumption.getConsumption())
                                        .build())
                                .collect(toList()))
                        .collect(toList()))
                .collect(toList())
                .stream()
                .flatMap(List::stream)
                .collect(toList())
                .stream()
                .flatMap(List::stream)
                .sorted(comparing(EnergyReportResult::getPeriod))
                .collect(Collectors.toList());
    }

    public List<EnergyReportResult> getEnergyReportResultsWithCalculatedCost(@NonNull final BigDecimal cost,
                                                                             @NonNull final List<EnergyReportResult> energyReportResults) {
        return energyReportResults
                .stream()
                .map(it -> EnergyReportResult.builder()
                        .period(it.getPeriod())
                        .valueOfConsumption(it.getValueOfConsumption())
                        .cost(it.getValueOfConsumption().multiply(cost))
                        .build())
                .collect(toList());
    }

    private BigDecimal sumWeeklyConsumptions(@NonNull final List<WeeklyConsumption> weeklyConsumptions) {
        return weeklyConsumptions.stream()
                .map(weeklyConsumption -> sumDailyConsumptions(weeklyConsumption.getDailyConsumptions()))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal sumDailyConsumptions(@NonNull final List<DailyConsumption> dailyConsumptions) {
        return dailyConsumptions
                .stream()
                .map(DailyConsumption::getConsumption)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private String buildWeeklyInfoString(YearMonth yearMonth, Integer weekNumber) {
        return "Week " + weekNumber + " of " + yearMonth.getMonth() + " " + yearMonth.getYear();
    }

    private String buildMonthlyInfoString(YearMonth yearMonth) {
        return yearMonth.getMonth() + " " + yearMonth.getYear();
    }
}

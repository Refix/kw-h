package pl.dawidczyk.finestmedia.backend.service;

import lombok.Value;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.dawidczyk.finestmedia.backend.client.FinestmediaEnergyApiClient;
import pl.dawidczyk.finestmedia.common.model.AccountTimeSeries;
import pl.dawidczyk.finestmedia.common.model.DailyConsumption;
import pl.dawidczyk.finestmedia.common.model.EnergyReport;
import pl.dawidczyk.finestmedia.common.model.HourConsumption;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class DailyConsumptionServiceTest {

    private static final Fixtures fixtures = new Fixtures();

    @Mock
    private FinestmediaEnergyApiClient finestmediaEnergyApiClient;

    @InjectMocks
    private DailyConsumptionService dailyConsumptionService;

    @Test
    public void shouldReturnDailyConsumption() {
        //given
        given(finestmediaEnergyApiClient.getEnergyReport(fixtures.start, fixtures.end)).willReturn(fixtures.energyReport);
        //when
        List<DailyConsumption> dailyConsumptions = dailyConsumptionService.getDailyConsumptions(fixtures.start, fixtures.end);
        //then
        assertThat(dailyConsumptions).isEqualTo(fixtures.dailyConsumptions);
    }

    @Value
    public static class Fixtures {
        LocalDate start = LocalDate.of(2019, 12, 1);
        LocalDate end = LocalDate.of(2019, 12, 25);

        ZonedDateTime exampleOfzonedDateTime1 =
                ZonedDateTime.of(2019, 12, 2, 12, 0, 0, 0, ZoneId.of("Europe/London"));
        ZonedDateTime exampleOfzonedDateTime2 =
                ZonedDateTime.of(2019, 12, 2, 13, 0, 0, 0, ZoneId.of("Europe/London"));
        ZonedDateTime exampleOfzonedDateTime3 =
                ZonedDateTime.of(2019, 12, 2, 14, 0, 0, 0, ZoneId.of("Europe/London"));

        HourConsumption hourConsumption1 = HourConsumption.builder()
                .consumption(BigDecimal.valueOf(0.25))
                .date(exampleOfzonedDateTime1)
                .build();

        HourConsumption hourConsumption2 = HourConsumption.builder()
                .consumption(BigDecimal.valueOf(0.13))
                .date(exampleOfzonedDateTime2)
                .build();

        HourConsumption hourConsumption3 = HourConsumption.builder()
                .consumption(BigDecimal.valueOf(0.57))
                .date(exampleOfzonedDateTime3)
                .build();

        List<HourConsumption> hourConsumptions = Arrays.asList(hourConsumption1, hourConsumption2, hourConsumption3);

        AccountTimeSeries accountTimeSeries = AccountTimeSeries.builder()
                .hourConsumption(hourConsumptions)
                .build();

        EnergyReport energyReport = EnergyReport.builder()
                .accountTimeSeries(accountTimeSeries)
                .build();

        DailyConsumption dailyConsumption1 = DailyConsumption.builder()
                .consumption(BigDecimal.valueOf(0.95))
                .date(LocalDate.of(2019, 12, 2))
                .build();

        List<DailyConsumption> dailyConsumptions = Collections.singletonList(dailyConsumption1);
    }
}
package pl.dawidczyk.finestmedia.backend.service;

import lombok.Value;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.dawidczyk.finestmedia.common.model.AggregatedEnergyReport;
import pl.dawidczyk.finestmedia.common.model.DailyConsumption;
import pl.dawidczyk.finestmedia.common.model.MonthlyConsumption;
import pl.dawidczyk.finestmedia.common.model.WeeklyConsumption;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class MonthlyWeeklyConsumptionServiceTest {

    private static final Fixtures fixtures = new Fixtures();

    @Mock
    private DailyConsumptionService dailyConsumptionService;

    @InjectMocks
    private MonthlyWeeklyConsumptionService monthlyWeeklyConsumptionService;

    @Test
    public void shouldReturnAggregatedEnergyReport() {
        //given
        given(dailyConsumptionService.getDailyConsumptions(fixtures.start, fixtures.end)).willReturn(fixtures.dailyConsumptions1);
        //when
        AggregatedEnergyReport aggregatedEnergyReport = monthlyWeeklyConsumptionService.getAggregatedEnergyReport(fixtures.start, fixtures.end);
        //then
        assertThat(aggregatedEnergyReport).isEqualTo(fixtures.aggregatedEnergyReport);
    }

    @Value
    public static class Fixtures {
        LocalDate start = LocalDate.of(2019, 12, 1);
        LocalDate end = LocalDate.of(2019, 12, 25);

        DailyConsumption dailyConsumption1 = DailyConsumption.builder()
                .consumption(BigDecimal.valueOf(0.95))
                .date(LocalDate.of(2019, 12, 2))
                .build();

        DailyConsumption dailyConsumption2 = DailyConsumption.builder()
                .consumption(BigDecimal.valueOf(0.95))
                .date(LocalDate.of(2019, 12, 3))
                .build();

        DailyConsumption dailyConsumption3 = DailyConsumption.builder()
                .consumption(BigDecimal.valueOf(0.95))
                .date(LocalDate.of(2019, 12, 4))
                .build();

        DailyConsumption dailyConsumption4 = DailyConsumption.builder()
                .consumption(BigDecimal.valueOf(0.95))
                .date(LocalDate.of(2019, 12, 5))
                .build();

        DailyConsumption dailyConsumption5 = DailyConsumption.builder()
                .consumption(BigDecimal.valueOf(0.95))
                .date(LocalDate.of(2019, 12, 6))
                .build();

        DailyConsumption dailyConsumption6 = DailyConsumption.builder()
                .consumption(BigDecimal.valueOf(0.95))
                .date(LocalDate.of(2019, 12, 7))
                .build();

        List<DailyConsumption> dailyConsumptions1 = Arrays.asList(
                dailyConsumption1,
                dailyConsumption2,
                dailyConsumption3,
                dailyConsumption4,
                dailyConsumption5,
                dailyConsumption6);

        WeeklyConsumption weeklyConsumption1 = WeeklyConsumption.builder()
                .yearMonth(YearMonth.of(2019, 12))
                .weekNumber(1)
                .dailyConsumptions(dailyConsumptions1)
                .build();

        List<WeeklyConsumption> weeklyConsumptions1 = Collections.singletonList(weeklyConsumption1);

        MonthlyConsumption monthlyConsumption1 = MonthlyConsumption.builder()
                .yearMonth(YearMonth.of(2019, 12))
                .weeklyConsumptions(weeklyConsumptions1)
                .build();

        List<MonthlyConsumption> monthlyConsumptions = Collections.singletonList(monthlyConsumption1);

        AggregatedEnergyReport aggregatedEnergyReport = AggregatedEnergyReport.builder()
                .monthlyConsumptions(monthlyConsumptions)
                .build();
    }
}